## Ansible with GITLAB Continuous integration and continuous delivery (CI/CD)

### Introduction
I have created this repository to learn how Gitlab CI/CD works, I choose Ansible to generate config files in json format by using the restconf. I have also used Jinja2 to convert YAML to JSON format for the variables defined in [loop_intf_config.yml](loop_intf_config.yml). For the test strategy, I have put couple of simple plays but in the real world we ideally use unit test or pyATS for network automation testing. As a part of CI, configuration file are created and will be save as artifacts which then use for configuration deployment as part of CD process.

You may also notice that, I have used one playbook to run all stages as I have tagged my plays related to the stages, however, in the real world, you may use different playbooks with different Dev, test and Prod environments but since this is something I am testing gitlab ci/cd, so we should be good to understand the work flow with one playbook.

### Gitlab Runner
I have used my own gitlab runner for this project in DevNet Expert CWS. [Here](gitlab-runner-install.md) is the process to download, install and running with own gitlab-runner instead of shared gitlab runners. 

### Installation
You may use fork or clone this repo using any of the link below
```bash
git clone git@gitlab.com:muhammadrafi/cicd_ansible_project.git
```
or

```bash
git clone https://gitlab.com/muhammadrafi/cicd_ansible_project.git
```

### Usage
The playbook used in project is to create two Loopback interfaces (100 and 101), however you add another interface after you clone the repo, this will trigger the pipeline after commit the change.

Here how you can add another Loopback interface 102 in [loop_intf_config.yml](loop_intf_config.yml)

```yaml
config:
  interface:
    - name: Loopback100
      description: configured via ansible and gitlab cicd
      type: iana-if-type:softwareLoopback
      enabled: true
      ietf-ip:ipv4:
        address:
          - ip: 10.100.100.1
            netmask: 255.255.255.255
    - name: Loopback101
      description: configured via ansible and gitlab cicd
      type: iana-if-type:softwareLoopback
      enabled: true
      ietf-ip:ipv4:
        address:
          - ip: 10.100.101.1
            netmask: 255.255.255.255

    # Add another Loopback interface 102
    - name: Loopback102
      description: configured via ansible and gitlab cicd
      type: iana-if-type:softwareLoopback
      enabled: true
      ietf-ip:ipv4:
        address:
          - ip: 10.100.102.1
            netmask: 255.255.255.255

```
once you stage, commit and push the change, it will trigger the pipeline.

```bash
git add . && git commit -m "added another loopback" && git push
```

Here are some screen shots from last comments I made to add two loopback interfaces. 

__Pipeline__

![](images/pipeline_1.png)
![](images/pipeline_2.png)

__config_build stage__

![](images/config_build.png)

__config_test stage__

![](images/config_test.png)

__config_deploy stage__

![](images/config_deploy.png)

__config_veify stage__

![](images/config_verify.png)

__Artifacts__

![](images/artifacts_1.png)
![](images/artifacts_2.png)

__CLI Verification__

![](images/cli_verify_1.png)
![](images/cli_verify_2.png)

### Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### License
[MIT](https://choosealicense.com/licenses/mit/)

### Author
[Muhammad Rafi](https://www.linkedin.com/in/muhammad-rafi-0a37a248/)