## Gitlab Runner in DevNet Expert CWS (Candidate Work Station)

### Introduction
GitLab Runner is an application or agent like service that helps in GitLab CI/CD to run jobs in a pipeline. By default, Gitlab provides shared runners when you create a project on gitlab along with .gitlab-ci.yml file and enable the CI/CD option on the project. 

Besides the shared runner, for security and more management/control purpose, Gitlab also encourages you to use your own gitlab runner. Therefore, in this post, I will showing you how to install and register gitlab-runner on DevNet Expert CWS. The process will be pretty much same for Ubunut, since DevNet Expert CWS is Ubuntu 20.04.3 LTS.

```bash
(ansible) expert@expert-cws:~/cicd_ansible_project$ lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 20.04.3 LTS
Release:	20.04
Codename:	focal
```

### Gitlab Runner Installation 
I will be using the binary file option as this is quick and simple process, but you can find other installation methods [Here](https://docs.gitlab.com/runner/install/linux-manually.html). 

**Step1:** Download the gitlab-runner binary in '/usr/local/bin/', you can choose the specific release or use the 'latest' binary. 

I pulled v14.10.0, since DevNet equipment requirements listed this. 

```bash
$ sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/v14.10.0/binaries/gitlab-runner-linux-amd64"
```

**Step2:** Change the permission of the gitlab-runner to make it executable 
```bash
$ sudo chmod +x /usr/local/bin/gitlab-runner
```

**Step3:** Create a GitLab Runner user for the gitlab-runner to use
```bash
$ sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```
**Step4:** Install and run as service with the gitlab user we created in previous step
```bash
$ sudo gitlab-runner install --user==gitlab-runner --working-directory=/home/gitlab-runner
```
**Step5:** Start the gitlab-runner 
```bash
$ sudo gitlab-runner start
```

Note: make sure you have /usr/local/bin/ is in your PATH for root

```
root@expert-cws:~# echo $PATH
/home/expert/venvs/ansible/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/snap/bin
```

### Gitlab Runner Verification
Check the gitlab-runner service is running 

```bash
$ sudo systemctl status gitlab-runner
[sudo] password for expert: 
● gitlab-runner.service - GitLab Runner
     Loaded: loaded (/etc/systemd/system/gitlab-runner.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2022-07-03 17:24:50 UTC; 2 days ago
   Main PID: 316512 (gitlab-runner)
      Tasks: 9 (limit: 4573)
     Memory: 14.8M
     CGroup: /system.slice/gitlab-runner.service
             └─316512 /usr/local/bin/gitlab-runner run --working-directory /home/gitlab-runner --config /etc/gitlab-runner/config.to>

Jul 05 16:42:10 expert-cws gitlab-runner[316512]: Job succeeded                                       duration_s=80.441318876 job=26>
```

Check the gitlab-runner version 

```bash
$ sudo gitlab-runner --version
Version:      14.10.0
Git revision: c6bb62f6
Git branch:   14-10-stable
GO version:   go1.17.7
Built:        2022-04-19T18:17:10+0000
OS/Arch:      linux/amd64
```

### Gitlab Runner Registration
There are two ways of registering the runner, one is interactive and other one is non-interactive. In registration, there are multiple options available to choose for the excutor, you can find more detail for the runner executors [here](https://docs.gitlab.com/runner/executors/index.html). I am using docker executor for my project. There are some limitation in Docker Executore, you find the details [here](https://docs.gitlab.com/runner/executors/docker.html)

For runner registration, you first need to get the registration token and Gitlab URL from your project.

Go to your project then CI/CD Settings> and then Expand Runners. 

e.g. `cicd_ansible_project > CI/CD Settings`

![](images/gitlab_cicd_path.png)

You will also need your Gitlab instance URL if you running your own Gitlab instance, otherwise for public Gitlab URL is https://gitlab.com/. I have created my project on public, so I will be using same. 

Whilst copying the URL and token, also disable the `Enable shared runners for this project` since you are going to be using your own runner. see the screenshot below.

![](images/runner_url_token.png)

Once you have your Gitlab URL and registration, you can now register your runner by using either of the following method.

Non-interactive registration with one line

```bash
$ sudo gitlab-runner register --non-interactive --name cws-gitlab-runner --executor 'docker' --url 'https://gitlab.com/' --tag-list ansible --docker-image 'ubuntu:latest' --registration-token 'GR1364721aPcrQYWxntyf6Mstuheoj'
```

For interactive registration, you can enter same details used in the above one line registration process. 

1. Run the command on the terminal `gitlab-runner register`
2. Enter your GitLab instance URL (also known as the gitlab-ci coordinator URL).
3. Enter the token you obtained to register the runner.
4. Enter a description for the runner. You can change this value later in the GitLab user interface.
5. Enter the tags associated with the runner, separated by commas. You can change this value later in the GitLab user interface.
6. Enter any optional maintenance note for the runner.
7. Provide the runner executor. For most use cases, enter docker.
If you entered docker as your executor, you are asked for the default image to be used for projects that do not define one in .gitlab-ci.yml.

```bash
(ansible) expert@expert-cws:~$ gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=316885 revision=c6bb62f6 version=14.10.0
WARNING: Running in user-mode.                     
WARNING: The user-mode requires you to manually start builds processing: 
WARNING: $ gitlab-runner run                       
WARNING: Use sudo for system-mode:                 
WARNING: $ sudo gitlab-runner...                   
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
GR1364721aPcrQYWxntyf6Mstuheoj
Enter a description for the runner:
[expert-cws]: csw-gitlab-runner
Enter tags for the runner (comma-separated):
ansible
Enter optional maintenance note for the runner:

Registering runner... succeeded                     runner=GR1348941aPcrQYWx
Enter an executor: custom, docker, docker-ssh+machine, ssh, virtualbox, docker+machine, kubernetes, docker-ssh, parallels, shell:
docker
Enter the default Docker image (for example, ruby:2.7):
ubuntu:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
```

Once you register the runner, run the following command to verify and list the runners, you can have multiple runners running on your system for different projects or command runner for multiple projects. 

Verify the Runner
```bash
(ansible) expert@expert-cws:~$ sudo gitlab-runner verify
Runtime platform                                    arch=amd64 os=linux pid=441686 revision=c6bb62f6 version=14.10.0
Running in system-mode.                            
                                                   
Verifying runner... is alive                        runner=s_37KXie
```

List the Runners
```bash
(ansible) expert@expert-cws:~$ sudo gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=441754 revision=c6bb62f6 version=14.10.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
cws-gitlab-runner                                   Executor=docker Token=s_37KXieXjykLuH8w6sy URL=https://gitlab.com/
```
Once you registered and verified the runner, you can now go to Gitlab CI/CD section again to verify runner is online and ready to use. 

![](images/runner_status.png)

Notice, green circle, shows runner is online and ready to use in your project. 

That's all for now, please let me know if you find any typos or missing information in this article. happy to take any suggestion if it make sense. 

To unregistered all the gitlab-runner 

`gitlab-runner unregister --all-runners`

`gitlab-runner verify --delete` 

```bash
(main) expert@expert-cws:~$ gitlab-runner unregister --all-runners
Runtime platform                                    arch=amd64 os=linux pid=13703 revision=c6bb62f6 version=14.10.0
WARNING: Running in user-mode.                     
WARNING: Use sudo for system-mode:                 
WARNING: $ sudo gitlab-runner...                   
                                                   
WARNING: Unregistering all runners                 
(main) expert@expert-cws:~$ sudo gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=13722 revision=c6bb62f6 version=14.10.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
(main) expert@expert-cws:~$ 

(main) expert@expert-cws:~$ sudo gitlab-runner verify --delete
Runtime platform                                    arch=amd64 os=linux pid=13797 revision=c6bb62f6 version=14.10.0
Running in system-mode.                            
                                                   
(main) expert@expert-cws:~$ 
```

For specific runner 
`gitlab-runner unregister runner_id/name`

### Author 
[Muhammad Rafi](https://www.linkedin.com/in/muhammad-rafi-0a37a248/)


### References
[GitLab Runner](https://docs.gitlab.com/runner/)

[Install GitLab Runner manually on GNU/Linux](https://docs.gitlab.com/runner/install/linux-manually.html)

[Registering runners](https://docs.gitlab.com/runner/register/index.html)

[Docker Executor](https://docs.gitlab.com/runner/executors/docker.html)

[Troubleshooting GitLab Runner](https://docs.gitlab.com/runner/faq/)

